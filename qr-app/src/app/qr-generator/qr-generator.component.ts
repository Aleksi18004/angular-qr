import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-qr-generator',
  templateUrl: './qr-generator.component.html',
  styleUrls: ['./qr-generator.component.css']
})
export class QrGeneratorComponent implements OnInit {

  @Input() qrCode: string;

  apiUrl: string;

  constructor() {
    this.qrCode = "Example";
    this.apiUrl = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=";
  }


  public get QrCode() : string {
    return this.qrCode;
  }
  public set QrCode(value: string) {
    this.qrCode = value;
  }


  async onSubmit(e: Event){
    e.preventDefault();

    let data = await fetch(`https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${this.qrCode}`);
    console.log(data);

  }

  ngOnInit(): void {
  }

}
